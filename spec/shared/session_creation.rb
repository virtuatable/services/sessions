# frozen_string_literal: true

RSpec.shared_examples 'POST /' do
  describe 'Creation of a session for an account' do
    let!(:account) { create(:random_account) }
    let!(:application) { create(:random_premium_app) }
    let!(:payload) { { app_key: application.app_key } }

    it_should_behave_like 'a route', 'post', '/'

    # Nominal scenario :
    # - The user provides en email address and a password
    # - The password corresponds to the email address
    # - The API creates a session and returns it
    describe 'The user successfully creates a session' do
      before do
        credentials = {
          email: account.email,
          password: 'super_secure_pwd'
        }
        post '/', payload.merge(credentials)
      end
      it 'Returns a 201 (Created) status code' do
        expect(last_response.status).to be 201
      end
      it 'Returns the correct body' do
        session = account.sessions.first
        expected = {
          session_id: session.session_id,
          created_at: session.created_at.utc.to_datetime.iso8601,
          duration: 86_400,
          account: {
            id: account.id.to_s,
            username: account.username,
            email: account.email
          }
        }
        expect(last_response.body).to include_json(expected)
      end
      it 'Has created a session for the user' do
        expect(account.sessions.count).to be 1
      end
    end

    # Alternative scenario :
    # - The user creates a first session, having never logged in prior to that
    # - The user creates a second session with the same account
    # - The API should have created two devices, the one linked to the first
    #   session should be safe, not the other one.
    describe 'The user creates a safe device only for the first session' do
      let!(:params) { payload.merge({ email: account.email, password: 'super_secure_pwd' }) }
      let!(:first_headers) do
        {
          'HTTP_USER_AGENT' => Faker::Internet.user_agent,
          'REMOTE_ADDR' => Faker::Internet.unique.ip_v4_address
        }
      end
      let!(:second_headers) do
        {
          'HTTP_USER_AGENT' => Faker::Internet.user_agent,
          'REMOTE_ADDR' => Faker::Internet.unique.ip_v4_address
        }
      end
      before do
        # First call, no session exist yet
        post '/', params, first_headers
        # Second call, this one should create another not safe device
        post '/', params, second_headers
      end
      it 'Has created the first device as safe' do
        expect(Arkaan::Authentication::Device.find_by(ip: first_headers['REMOTE_ADDR']).safe).to be true
      end
      it 'Has created the second device as not safe' do
        expect(Arkaan::Authentication::Device.find_by(ip: second_headers['REMOTE_ADDR']).safe).to be false
      end
    end

    # Alternative case :
    # - Everything goes as in the nominal case
    # - The user asks for a specific session duration as additional parameter
    # - The API creates a session and returns it
    describe 'The user creates a session with a specified duration' do
      before do
        credentials = {
          email: account.email,
          password: 'super_secure_pwd',
          duration: 3600
        }
        post '/', payload.merge(credentials)
      end
      it 'Returns a 201 (Created) status code' do
        expect(last_response.status).to be 201
      end
      it 'Returns the correct body' do
        session = account.sessions.first
        expected = {
          session_id: session.session_id,
          created_at: session.created_at.utc.to_datetime.iso8601,
          duration: 3600,
          account: {
            id: account.id.to_s,
            username: account.username,
            email: account.email
          }
        }
        expect(last_response.body).to include_json(expected)
      end
      it 'Has created a session for the user' do
        expect(account.sessions.count).to be 1
      end
    end

    # Alternative case :
    # - Everything goes as in the nominal case
    # - The user requests the session to never end by passing a zero duration
    # - The API creates a session and returns it
    describe 'The user creates a session with a zero duration' do
      before do
        credentials = {
          email: account.email,
          password: 'super_secure_pwd',
          duration: 0
        }
        post '/', payload.merge(credentials)
      end
      it 'Returns a 201 (Created) status code' do
        expect(last_response.status).to be 201
      end
      it 'Returns the correct body' do
        session = account.sessions.first
        expected = {
          session_id: session.session_id,
          created_at: session.created_at.utc.to_datetime.iso8601,
          duration: 0,
          account: {
            id: account.id.to_s,
            username: account.username,
            email: account.email
          }
        }
        expect(last_response.body).to include_json(expected)
      end
      it 'Has created a session for the user' do
        expect(account.sessions.count).to be 1
      end
    end

    # Alternative case :
    # - Everything goes as in the nominal case
    # - The user passes an invalid duration (any string really)
    # - The API creates a session and returns it
    #
    # NOTE :
    # This is not an exception scenario as in this case the session should just never end.
    describe 'The user creates a session with an invalid duration' do
      before do
        credentials = {
          email: account.email,
          password: 'super_secure_pwd',
          duration: 'test'
        }
        post '/', payload.merge(credentials)
      end
      it 'Returns a 201 (Created) status code' do
        expect(last_response.status).to be 201
      end
      it 'Returns the correct body' do
        session = account.sessions.first
        expected = {
          session_id: session.session_id,
          created_at: session.created_at.utc.to_datetime.iso8601,
          duration: 0,
          account: {
            id: account.id.to_s,
            username: account.username,
            email: account.email
          }
        }
        expect(last_response.body).to include_json(expected)
      end
      it 'Has created a session for the user' do
        expect(account.sessions.count).to be 1
      end
    end

    # Alternative scenario :
    # - The user provides all the necessary informations
    # - The application can access its IP address and user agent
    # - The password match and the account is valid
    # - The API creates a session, a device, and returns the session
    describe 'The user creates a session with additional informations' do
      let!(:user_agent) { Faker::Internet.user_agent }
      let!(:headers) do
        {
          'HTTP_USER_AGENT' => user_agent,
          'REMOTE_ADDR' => Faker::Internet.ip_v4_address
        }
      end

      before do
        credentials = {
          email: account.email,
          password: 'super_secure_pwd'
        }
        post '/', payload.merge(credentials), headers
      end
      it 'Returns a 201 (Created) status code' do
        expect(last_response.status).to be 201
      end
      it 'Creates the device' do
        expect(Arkaan::Authentication::Device.count).to be 1
      end
      describe 'The created device' do
        let!(:device) { Arkaan::Authentication::Device.first }

        it 'has a user agent correctly set' do
          expect(device.user_agent).to eq user_agent
        end
        it 'has an IP address correctly set' do
          expect(device.ip).to eq headers['REMOTE_ADDR']
        end
        it 'has only one session in its sessions list' do
          expect(device.sessions.count).to be 1
        end
        it 'has the correct session in its sessions list' do
          expect(device.sessions.first.account.id.to_s).to eq account.id.to_s
        end
      end
    end

    # Alternative scenario :
    # - The user provides all the necessary informations
    # - The application can not access its IP address and user agent
    # - The password and its confirmation match, the account is valid
    # - The API creates a session, an empty device, and returns the session
    describe 'The user creates a session without informations' do
      before do
        credentials = {
          email: account.email,
          password: 'super_secure_pwd'
        }
        post '/', payload.merge(credentials)
      end
      it 'Returns a 201 (Created) status code' do
        expect(last_response.status).to be 201
      end
      it 'Has created a device' do
        expect(Arkaan::Authentication::Device.count).to be 1
      end
      describe 'Created device' do
        let!(:device) { Arkaan::Authentication::Device.first }

        it 'Has a correct IP address' do
          expect(device.ip).to eq '127.0.0.1'
        end
        it 'Has a correct user agent' do
          expect(device.user_agent).to eq ''
        end
      end
    end

    # Alternative scenario :
    # - The user provides all the necessary informations
    # - The application can only access user agent, not IP address
    # - The password and its confirmation match, the account is valid
    # - The API creates a session, an empty device, and returns the session
    #
    # NOTE :
    # There is not scenario on "only user agent and no IP" because the IP address
    # is always set, defaulting to 127.0.0.1, therefore it's handled in the display
    # where this default value is not returned, but an empty string is instead.
    describe 'The user only provides an IP address' do
      let!(:headers) { { 'REMOTE_ADDR' => Faker::Internet.ip_v4_address } }
      before do
        credentials = {
          email: account.email,
          password: 'super_secure_pwd'
        }
        post '/', payload.merge(credentials), headers
      end

      it 'Returns a 201 (Created) status code' do
        expect(last_response.status).to be 201
      end
      it 'Creates the device' do
        expect(Arkaan::Authentication::Device.count).to be 1
      end
      describe 'The created device' do
        let!(:device) { Arkaan::Authentication::Device.first }

        it 'has a user agent correctly set' do
          expect(device.user_agent).to eq ''
        end
        it 'has an IP address correctly set' do
          expect(device.ip).to eq headers['REMOTE_ADDR']
        end
      end
    end

    # Alternative scenario :
    # - Everything goes as in the nominal scenario for the session creation
    # - The device the user is connecting from already exists
    # - The session is correctly created, but the device is not duplicated
    describe 'The device the user is connecting from already exists' do
      let!(:device) { create(:random_device) }
      let!(:headers) do
        {
          'HTTP_USER_AGENT' => device.user_agent,
          'REMOTE_ADDR' => device.ip
        }
      end

      before do
        credentials = {
          email: account.email,
          password: 'super_secure_pwd'
        }
        post '/', payload.merge(credentials), headers
      end
      it 'Returns a 201 (Created) status code' do
        expect(last_response.status).to be 201
      end
      it 'Has not created another device' do
        expect(Arkaan::Authentication::Device.count).to be 1
      end
      it 'Has added a session to the device' do
        expect(device.sessions.count).to be 1
      end
    end

    # Error scenario :
    # - The user does not provide an email address in the payload
    # - The API fails and returns an error code
    describe 'The email address is not provided' do
      before do
        post '/', payload.merge({ password: 'anything' })
      end
      it 'Returns a 400 (Bad Request) status code' do
        expect(last_response.status).to be 400
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 400,
          field: 'email',
          error: 'required'
        )
      end
    end

    # Error scenario :
    # - The user does not provide a password in the payload
    # - The API fails and returns an error code
    describe 'The password is not provided' do
      before do
        post '/', payload.merge({ email: 'anything@mail.com' })
      end
      it 'Returns a 400 (Bad Request) status code' do
        expect(last_response.status).to be 400
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 400,
          field: 'password',
          error: 'required'
        )
      end
    end

    # Error scenario :
    # - The user makes a request and provides both email address and password
    # - The email address does not exist in the database
    # - The API fails and returns an error code
    describe 'The account ID provided does not exists' do
      before do
        post '/', payload.merge({ email: 'unknown', password: 'test' })
      end
      it 'Returns a 404 (Not Found) status code' do
        expect(last_response.status).to be 404
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 404,
          field: 'email',
          error: 'unknown'
        )
      end
    end

    # Error scenario :
    # - The user makes a request and provides both email address and password
    # - The password does not correspond to the email address
    # - The API fails and returns an error code
    describe 'The provided email and password do not match' do
      before do
        post '/', payload.merge({ email: account.email, password: 'test' })
      end
      it 'Returns a 403 (Forbidden) status code' do
        expect(last_response.status).to be 403
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 403,
          field: 'password',
          error: 'wrong'
        )
      end
    end
  end
end
