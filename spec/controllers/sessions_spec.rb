# frozen_string_literal: true

RSpec.describe Controllers::Sessions do
  def app
    Controllers::Sessions
  end

  include_examples 'DELETE /:id'

  include_examples 'GET /'

  include_examples 'POST /'
end
