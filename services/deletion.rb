# frozen_string_literal: true

module Services
  # This service handles deleting a session, without returning an error if
  # the session does not exist to keep indempotency.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Deletion
    include Singleton

    # This method is used to delete sessions based on the :id field passed
    # in the URL. DO NOT use the session_id field that is the current session ID
    # of the user requesting the deletion.
    # @param parameters [Hash] the parameters passed by the controller
    # @param account [Arkaan::Account] the account requestion the deletion
    def run(parameters, account)
      search = { session_id: parameters['id'] }
      account.sessions.where(search).first&.delete
    end
  end
end
