# frozen_string_literal: true

RSpec.describe Decorators::Device do
  describe 'When the IP has the default value' do
    let!(:device) { create(:random_device, ip: '127.0.0.1').enhance }
    let!(:browser) { Browser.new device.user_agent }

    it 'Returns the correct hash representation' do
      expected = {
        browser: browser.name,
        platform: browser.platform.name,
        ip: ''
      }
      expect(device.to_h).to include_json expected
    end
  end

  describe 'When the IP has a standard value' do
    let!(:device) { create(:random_device).enhance }
    let!(:browser) { Browser.new device.user_agent }

    it 'Returns the correct hash representation' do
      expected = {
        browser: browser.name,
        platform: browser.platform.name,
        ip: device.ip
      }
      expect(device.to_h).to include_json expected
    end
  end
end
