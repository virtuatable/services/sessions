# frozen_string_literal: true

# Main module for all the controllers of the web application
# @author Vincent Courtois <courtois.vincent@outlook.com>
module Controllers
  # Insert more informations about your controller here.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Sessions < Virtuatable::Controllers::Base
    api_route 'post', '/', options: { authenticated: false, premium: true } do
      check_presence 'email', 'password'
      api_created Services::Creation.instance.run(params, request)
    end

    api_route 'delete', '/:id', options: { premium: true } do
      Services::Deletion.instance.run(params, account)
      api_empty
    end

    api_route 'get', '/' do
      api_list Services::Listing.instance.run(session), :with_device
    end
  end
end
