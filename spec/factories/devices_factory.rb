# frozen_string_literal: true

FactoryBot.define do
  factory :empty_devices, class: Arkaan::Authentication::Device do
    factory :random_device do
      ip { Faker::Internet.ip_v4_address }
      user_agent { Faker::Internet.user_agent }
    end
  end
end
