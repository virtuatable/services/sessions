# frozen_string_literal: true

module Services
  # This class lists the sessions of a user from its currently logged in session.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Listing
    include Singleton

    # Lists the current sessions for the user linked to this session.
    # @param session [Arkaan::Authentication::Session] the session to get the informations from
    # @return [Array<Arkaan::Authentication::Session>] the sessions of the corresponding user
    def run(session)
      session.account.sessions.reject(&:expired?)
    end
  end
end
