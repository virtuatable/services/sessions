# frozen_string_literal: true

require 'bundler'
Bundler.require :runtime

Virtuatable::Application.load!('sessions')

run Controllers::Sessions
