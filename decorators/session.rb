# frozen_string_literal: true

module Decorators
  # Decorator of a session. Account hash representation is embedded
  # because having its own decorator would not really be useful.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Session < Virtuatable::Enhancers::Base
    enhances Arkaan::Authentication::Session

    def to_h
      {
        session_id: session_id,
        duration: duration,
        created_at: created_at,
        account: {
          id: account.id.to_s,
          email: account.email,
          username: account.username
        }
      }
    end

    def with_device
      {
        session_id: session_id,
        device: device.enhance.to_h
      }
    end

    private

    def created_at
      object.created_at.utc.to_datetime.iso8601
    end
  end
end
