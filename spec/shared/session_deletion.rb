# frozen_string_literal: true

RSpec.shared_examples 'DELETE /:id' do
  describe 'Deletion of a session' do
    let!(:account) { create(:random_administrator) }
    let!(:session) { create(:random_session, account: account) }
    let!(:application) { create(:random_premium_app, creator: account) }
    let!(:payload) { { app_key: application.app_key, session_id: session.session_id } }

    it_should_behave_like 'a route', 'delete', '/:id'

    # Nominal scenario :
    # - The user wants to delete a session because he logged off
    # - The session exists in the database
    # - The API deletes the session and returns a standard deletion message
    describe 'When the session is correctly deleted' do
      before do
        delete "/#{session.session_id}", payload
      end
      it 'Returns a 204 (No Content) status code' do
        expect(last_response.status).to be 204
      end
      it 'Returns no body' do
        expect(last_response.body).to eq ''
      end
      it 'Has deleted the session correctly' do
        expect(Arkaan::Authentication::Session.count).to be 0
      end
    end

    # Alternative scenario :
    # - The user wants to delete a session because he logged off
    # - The session does not exist in the database
    # - The API does not delete it, but returns a standard deletion message
    # Notes :
    # - This is not an exception scenario, even if the session does not exist
    # - This behaviour is done for indempotency purposes
    describe 'When the session_id does not exist' do
      before do
        delete '/unknown', payload
      end
      it 'Returns a 204 (No Content) status code' do
        expect(last_response.status).to be 204
      end
      it 'Returns no body' do
        expect(last_response.body).to eq ''
      end
    end

    # Alternative scenario :
    # - The user wants to delete a session that does not belong to him
    # - The session exists, but belongs to another user
    # - The API does not delete it, but returns a standard deletion message
    # Notes :
    # - This is not an exception scenario
    # - This behaviour is done for indempotency purposes
    describe 'When the session_id does not exist' do
      let!(:other_account) { create(:random_account) }
      let!(:other_session) { create(:random_session, account: other_account) }

      before do
        delete "/#{other_session.id}", payload
      end
      it 'Returns a 204 (No Content) status code' do
        expect(last_response.status).to be 204
      end
      it 'Returns no body' do
        expect(last_response.body).to eq ''
      end
      it 'Has not deleted the session' do
        search = { session_id: session.session_id }
        expect(Arkaan::Authentication::Session.where(search).count).to be 1
      end
    end
  end
end
