# frozen_string_literal: true

RSpec.shared_examples 'GET /' do
  describe 'Listing of sessions for an account' do
    it_should_behave_like 'a route', 'get', '/'

    let!(:account) { create(:random_administrator) }
    let!(:application) { create(:random_premium_app) }
    let!(:device) { create(:random_device) }
    let!(:session) { create(:random_session, account: account, device: device) }
    let!(:payload) do
      {
        app_key: application.app_key,
        session_id: session.session_id
      }
    end
    let!(:browser) { Browser.new(device.user_agent).name }
    let!(:platform) { Browser.new(device.user_agent).platform.name }

    # Nominal scenario :
    # - A user requests the list of his existing sessions
    # - The API correctly returns the list of sessions
    describe 'The user successfully gets his sessions' do
      before do
        get '/', payload
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          count: 1,
          items: [
            {
              session_id: session.session_id.to_s,
              device: {
                ip: device.ip,
                browser: browser,
                platform: platform
              }
            }
          ]
        )
      end
    end

    # Alternative scenario :
    # - A user requests his not yet expired sessions
    # - The API correctly returns the desired list of sessions
    describe 'The user successfully gets his active sessions' do
      let!(:expired_payload) do
        {
          account: account,
          duration: 3600,
          created_at: DateTime.now - 2.0 / 24
        }
      end
      let!(:expired) { create(:random_session, expired_payload) }

      before do
        get '/', payload
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          count: 1,
          items: [
            {
              session_id: session.session_id.to_s,
              device: {
                ip: device.ip,
                browser: browser,
                platform: platform
              }
            }
          ]
        )
      end
    end

    # Alternative scenario :
    # - A user requests the list of his sessions
    # - There are sessions for several users in the database
    # - The API only returns a list with this user's sessions
    describe 'The user gets only his session' do
      let!(:other_account) { create(:random_account) }
      let!(:other_session) { create(:random_session, account: other_account) }

      before do
        get '/', payload
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          count: 1,
          items: [
            {
              session_id: session.session_id.to_s,
              device: {
                ip: device.ip,
                browser: browser,
                platform: platform
              }
            }
          ]
        )
      end
    end
  end
end
