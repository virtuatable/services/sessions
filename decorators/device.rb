# frozen_string_literal: true

module Decorators
  # Decorator for a device, cleaning some of the default values for
  # IP address and formatting the user agent to be more readable.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Device < Virtuatable::Enhancers::Base
    enhances Arkaan::Authentication::Device

    def to_h
      {
        ip: ip,
        browser: browser.name,
        platform: browser.platform.name
      }
    end

    private

    def browser
      Browser.new(object.user_agent)
    end

    def ip
      object.ip == '127.0.0.1' ? '' : object.ip
    end
  end
end
