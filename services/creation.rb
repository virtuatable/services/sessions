# frozen_string_literal: true

module Services
  # This service holds the creation of sessions to authenticate users on the app
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Creation
    include Singleton

    DEFAULT_DURATION = 86_400

    # Creates the session with the given parameters.
    # @param parameters [Hash] the parameters given in the controller.
    # @return [Arkaan::Authentication::Session] the created session
    def run(parameters, request)
      account = Arkaan::Account.find_by(email: parameters['email'])
      check_password(account, parameters['password'])

      Arkaan::Authentication::Session.create(
        session_id: BSON::ObjectId.new.to_s,
        account: account,
        device: device_from(request, account),
        duration: parameters.fetch('duration', DEFAULT_DURATION).to_i
      )
    end

    private

    # Checks the given password to see if it matches the given account's password
    # @param account [Arkaan::Account] the account to check onto
    # @param password [String] the password to check on the account
    # @raise [Virtuatable::API::Errors::Forbidden] an error if the password does
    #   not match the account's password
    def check_password(account, password)
      authentication = account.authenticate(password)
      return unless authentication == false

      raise Virtuatable::API::Errors::Forbidden.new(
        field: 'password',
        error: 'wrong'
      )
    end

    # Gets a device from a given Sinatra request Ip address and user agent.
    # @param request [Sinatra::Request] the Sinatra request to get informations from.
    # @param account [Arkaan::Account] the account of the user creating this device
    def device_from(request, account)
      existing_set = Arkaan::Authentication::Device.where(
        user_agent: request.env.fetch('HTTP_USER_AGENT', ''),
        ip: request.env.fetch('REMOTE_ADDR', '127.0.0.1')
      )
      if existing_set.count.zero?
        create_device_from(request, account)
      else
        existing_set.first
      end
    end

    def create_device_from(request, account)
      Arkaan::Authentication::Device.create(
        user_agent: request.env.fetch('HTTP_USER_AGENT', ''),
        ip: request.env.fetch('REMOTE_ADDR', '127.0.0.1'),
        safe: account.sessions.count.zero?
      )
    end
  end
end
